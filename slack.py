import os
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
import pydle
import time
import json
import threading
import asyncio

# Initializes Slack app with your bot token and socket mode handler
app = App(token="xoxb-TOKEN")

#gets user ids and spins them into a dictionary for slack, if a user dosen't exist clearly they are new so reload
#note that this will fail if we have too many users join slack. If so we will need to pageante
userlist = {m['id']: m['name'] for m in app.client.users_list()['members']}
def lookupUser(id):
    global userlist
    if id not in userlist:
        userlist = {m['id']: m['name'] for m in app.client.users_list()['members']}
        if id not in userlist:
            return id
    return userlist[id]

#this gets called when there are new slack messages. this will post to IRC.
@app.event("message")
def thinghappened(event, say):
    suser = event["user"]
    slacktext=event["text"]
    tosend= f"{lookupUser(suser)}- {slacktext}"
    asyncio.run(ircClient.message("#cialug", tosend)) #yes I know that this is blocking... I was feeling lazy, someone else can fix this. ¯\_(ツ)_/¯ 

#this class extends the pydle IRC library. on connection to irc server it joins the cialug channel
#On message is called when there is new posts in the channel. They are then cross posted into general. Note the source.startswith. If there is a name collision 
#in irc the server will append a _ on the name causing it to miss and create an echo
class MyOwnBot(pydle.Client):
    async def on_connect(self):
        await self.join('#cialug')
    async def on_message(self, target, source, message):
        if not (source.startswith('LugBot')):
            app.client.chat_postMessage(channel="general", text=f"{source}: {message}")

#spins the slack bot off to a seperate thread, and also opens it via web socket mode. Note that you will have to grant the slack app privs when you create the tokens
class IRCBot(threading.Thread):
    def run(self):
        SocketModeHandler(app, "xapp-KEYGOESHERE").start()

# Starts off the slack in a seperate thread and then connects to IRC as Lugbot
if __name__ == "__main__":
    ircthread=IRCBot()
    ircthread.start()
    global ircClient
    ircClient = MyOwnBot('LugBot', realname='Lugbot')
    ircClient.run('irc.libera.chat', tls=True, tls_verify=False)
